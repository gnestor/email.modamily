# modamily.com
http://www.modamily.com

## Install
npm install

## Run
Test: `grunt test`
Build: `grunt build`
Build for Mailchimp: `grunt mc`
Build for Mandrill: `grunt mandrill`

## Notes
* Image `src` must be absolute URLs to images hosted on the web. Running `grunt mc` will inline the images as base64.
* Compass is not being used and Compass tasks have been commented out of the Gruntfile.
